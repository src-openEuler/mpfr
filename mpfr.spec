Name: mpfr
Version: 4.2.1
Release: 2
Summary: A C library for multiple-precision floating-point computations
URL: https://www.mpfr.org/
License: LGPL-3.0-or-later and GPL-3.0-or-later and GFDL-1.2-only
BuildRequires: gcc make
BuildRequires: gmp-devel >= 1:5.0
Requires: gmp >= 1:5.0

Source0: https://www.mpfr.org/%{name}-%{version}/%{name}-%{version}.tar.xz

%description
MPFR is a C library for arbitrary-precision binary floating-point computation 
with correct rounding, based on Multi-Precision Library. The computation is 
both efficient and has a well-defined semantics: the functions are completely 
specified on all the possible operands and the results do not depend on the 
platform. This is done by copying the ideas from the ANSI/IEEE-754 standard 
for fixed-precision floating-point arithmetic (correct rounding and exceptions, 
in particular)

%package devel
Summary: Development files for the MPFR library
Requires: %{name} = %{version}-%{release}

%description devel
Devel for %{name}

If you want to develop applications which will use the MPFR library,
you'll need to install the mpfr-devel package. You'll also need to
install the mpfr package.

%prep
%autosetup -p1

%build
%configure --disable-assert --disable-static
%make_build

%install
%make_install
%delete_la
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

rm -f $RPM_BUILD_ROOT%{_pkgdocdir}/COPYING  $RPM_BUILD_ROOT%{_pkgdocdir}/COPYING.LESSER

%check
%make_build check

%files
%license COPYING COPYING.LESSER
%doc NEWS README AUTHORS BUGS TODO doc/FAQ.html
%{_libdir}/libmpfr.so.*

%files devel
%{_pkgdocdir}/examples
%{_libdir}/libmpfr.so
%{_includedir}/*.h
%{_infodir}/mpfr.info*
%{_libdir}/pkgconfig/mpfr.pc

%changelog
* Tue Aug 06 2024 Funda Wang <fundawang@yeah.net> - 4.2.1-2
- Cleanup spec
- bump epoch for gmp package

* Mon Feb 26 2024 Liu Chao <liuchao173@huawei.com> - 4.2.1-1
- Upgrade to 4.2.1
  - Bug fixes
  - Improved MPFR manual
  - Configure tests

* Mon Nov 27 2023 Wenyu Liu<liuwenyu7@huawei.com> - 4.2.0-3
- Add upstream patch for 4.2.0.

* Fri Aug 4 2023 Wenyu Liu<liuwenyu7@huawei.com> - 4.2.0-2
- Modified a buggy test of the thousands separator. 

* Fri Aug 4 2023 Wenyu Liu<liuwenyu7@huawei.com> - 4.2.0-1
- update to 4.2.0

* Thu Aug 4 2022 Chenyx <chenyixiong3@huawei.com> - 4.1.0-3
- License compliance rectification

* Fri Apr 01 2022 zhouwenpei<zhouwenpei1@h-partners.com> - 4.1.0-2
- delete old so file

* Fri Jul 24 2020 jinzhimin<jinzhimin2@huawei.com> - 4.1.0-1
- update to 4.1.0

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.6-3
- Package init
